package ru.appmat.faculty

import io.ktor.application.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.Routing
import io.ktor.routing.routing
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
fun Application.facultyModule() {
    val facultyService = FacultyService(FacultyDAO(FacultyTable))
    routing {
        faculty(facultyService)
    }
}

internal fun Routing.faculty(facultyService: FacultyService) {
    get<API.Faculties> {
        val facList = facultyService.list()
        call.respond(facList)
    }

    post<API.Faculties> {
        val createFacultyCommand = call.receive<CreateFacultyCommand>()
        val createdFaculty = facultyService.create(createFacultyCommand)
        call.respond(Created, createdFaculty)
    }

    get<API.Faculties.Faculty> {
        val faculty = facultyService.load(it.id)
        call.respond(faculty)
    }

    put<API.Faculties.Faculty> {
        val faculty = call.receive<Faculty>()
        facultyService.update(faculty)
        call.respond(OK)
    }

    delete<API.Faculties.Faculty> {
        facultyService.delete(it.id)
        call.respond(OK)
    }
}
