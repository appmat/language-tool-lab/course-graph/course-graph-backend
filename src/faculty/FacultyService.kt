package ru.appmat.faculty

import org.jetbrains.exposed.sql.transactions.transaction

class FacultyService(private val facultyDAO: FacultyDAO) {

    fun create(createCommand: CreateFacultyCommand) = transaction {
        val facultyId = facultyDAO.persist(Faculty(-1, createCommand.name))
        facultyDAO.load(facultyId)
    }

    fun list() = transaction {
        facultyDAO.list()
    }

    fun load(id: Long) = transaction {
        facultyDAO.load(id)
    }

    fun delete(id: Long) = transaction {
        facultyDAO.delete(id)

    }

    fun update(faculty: Faculty) = transaction {
        facultyDAO.update(faculty)
    }
}
