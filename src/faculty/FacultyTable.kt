package ru.appmat.faculty

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column

object FacultyTable : LongIdTable("faculty") {
    val name: Column<String> = varchar("name", 150)
}
