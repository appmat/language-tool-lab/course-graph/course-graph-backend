package ru.appmat.faculty

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

class FacultyDAO(private val facultyTable: FacultyTable) {
    private fun ResultRow.toFaculty()= Faculty(
            id=this[FacultyTable.id].value,
            name=this[FacultyTable.name]
    )

    private fun UpdateBuilder<*>.fromFaculty(faculty: Faculty){
        this[FacultyTable.name]=faculty.name
    }

    fun load(id: Long) =
            facultyTable.select{
                FacultyTable.id eq id
            }.single().toFaculty()

    fun list()=facultyTable.selectAll().mapNotNull {
        it.toFaculty()
    }

    fun persist(faculty: Faculty)=facultyTable.insertAndGetId {
        it[name] = faculty.name
    }.value

    fun update(faculty: Faculty)=facultyTable.update({
        FacultyTable.id eq faculty.id
    }){
        it.fromFaculty(faculty)
    }

    fun delete(id: Long)=facultyTable.deleteWhere{
        FacultyTable.id eq id
    }
}
