package ru.appmat.faculty

data class Faculty(val id: Long, val name: String)

data class CreateFacultyCommand(val name: String)
