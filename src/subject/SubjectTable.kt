package ru.appmat.subject

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column

internal object SubjectTable : LongIdTable("subject"){
    val name: Column<String> = varchar("name", 150)
    val description: Column<String> = varchar("description", 350)
}