package ru.appmat.subject

data class Subject(
        val id:Long,
        val name:String,
        val description:String
) {

}