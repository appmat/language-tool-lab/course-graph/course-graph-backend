package ru.appmat.specialty

import io.ktor.application.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.Routing
import io.ktor.routing.routing
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
fun Application.specialtyModule() {
    val specialtyService = SpecialtyService(SpecialtyDAO(SpecialtyTable, CourseToSpecialtyTable))
    routing {
        specialty(specialtyService)
    }
}

internal fun Routing.specialty(specialtyService: SpecialtyService) {
    // получение всех специальностей
    get<API.Specialties> {
        val specialtyList = specialtyService.list()
        call.respond(specialtyList)
    }

    // создание новой специальности (без доп инфы)
    post<API.Specialties> {
        val createSpecialtyCommand = call.receive<CreateSpecialtyCommand>()
        val createdSpecialty = specialtyService.create(createSpecialtyCommand)
        call.respond(Created, createdSpecialty)
    }

    // получение специальности со всей сопутствующей инфой
    get<API.Specialties.Specialty> {
        val specialty = specialtyService.load(it.id)
        call.respond(specialty)
    }

    // обновление специальности (только название и факультет)
    put<API.Specialties.Specialty> {
        val specialty = call.receive<SpecialtyInfo>()
        specialtyService.updateInfo(specialty)
        call.respond(OK)
    }

    // удаление специальности из таблицы specialty
    delete<API.Specialties.Specialty> {
        specialtyService.delete(it.id)
        call.respond(OK)
    }

    put<API.Specialties.Specialty.Courses> {
        val courses = call.receive<Array<SpecialtyCourse>>()
        specialtyService.updateCourses(it.parent.id, courses)
        call.respond(OK)
    }

    get<API.Courses.Course.Specialties>{
        val specialties = specialtyService.loadByCourse(it.parent.id)
        call.respond(specialties)
    }

    // получение специальностей по id факультета
    get<API.Faculties.Faculty.Specialties> {
        val specialties = specialtyService.loadByFaculty(it.parent.id)
        call.respond(specialties)
    }

}
