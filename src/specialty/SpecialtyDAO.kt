package ru.appmat.specialty

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import ru.appmat.course.CourseTable


class SpecialtyDAO(
    private val specialtyTable: SpecialtyTable,
    private val courseToSpecialtyTable: CourseToSpecialtyTable,
    //private val courseTable: CourseTable
) {
    // получение информации в нужном виде
    private fun ResultRow.toSpecialtyInfo() = SpecialtyInfo(
            id=this[SpecialtyTable.id].value,
            name=this[SpecialtyTable.name],
            facId=this[SpecialtyTable.facId]
    )
    private fun ResultRow.toSpecialtyCourse() = SpecialtyCourse(
        courseId = this[CourseToSpecialtyTable.courseId],
        term = this[CourseToSpecialtyTable.term]
    )

    // для обновления информации в таблицах
    private fun UpdateBuilder<*>.fromSpecialtyInfo(specialtyInfo: SpecialtyInfo){
        this[SpecialtyTable.name] = specialtyInfo.name
        this[SpecialtyTable.facId] = specialtyInfo.facId
    }

    // получение одной специальности вместе с ее курсами
    fun load(id: Long): Specialty {
        val specialty = specialtyTable.select { SpecialtyTable.id eq id }.single().toSpecialtyInfo()
        val specialtyCourses = courseToSpecialtyTable.select{
            CourseToSpecialtyTable.specialtyId eq id
        }.mapNotNull {
            it.toSpecialtyCourse()
        }.toSet()
        return Specialty(specialty, specialtyCourses)
    }

    // получение всех специальностей
    // по идее, можно и не возвращать список курсов
    fun list() = specialtyTable.selectAll().mapNotNull {
        it.toSpecialtyInfo()
    }

    // добавление новой специальности, включает в себя только основную информацию
    fun persist(specialtyInfo: SpecialtyInfo) = specialtyTable.insertAndGetId {
        it[name] = specialtyInfo.name
        it[facId] = specialtyInfo.facId
    }.value

    // обновление специальности, без курсов и тд
    fun updateInfo(specialty: SpecialtyInfo) = specialtyTable.update({
            SpecialtyTable.id eq specialty.id
        }) {
            it.fromSpecialtyInfo(specialty)
        }

    fun delete(id: Long) = specialtyTable.deleteWhere {
        SpecialtyTable.id eq id
    }

    // получение специальностей по факультету, возвращает основную информацию (без курсов и тд)
    fun loadByFaculty(facId:Long) = specialtyTable.select {
        SpecialtyTable.facId eq facId
    }.mapNotNull {
        it.toSpecialtyInfo()
    }

    fun loadByCourse(courseId: Long) = (courseToSpecialtyTable innerJoin specialtyTable)
        .slice(specialtyTable.columns)
        .select{
            CourseToSpecialtyTable.courseId eq courseId
        }.mapNotNull {
            it.toSpecialtyInfo()
        }

    // обновление курсов специальности
    fun updateCourses(id: Long, courses: Array<SpecialtyCourse>){
        // текущие курсы специальности
        val currentCourses = courseToSpecialtyTable.select{
            CourseToSpecialtyTable.specialtyId eq id
        }.mapNotNull {
            it.toSpecialtyCourse()
        }
        // поступившая информация
        val newCourses = courses.map { it }

        // выделим устаревшие курсы и их id
        val coursesToDelete = currentCourses.minus(newCourses)
        val coursesToDeleteId = coursesToDelete.map{it.courseId}

        // и новые курсы, которые надо добавить
        val coursesToAdd = newCourses.minus(currentCourses)

        // удаляем те курсы, которые не указаны в newCourses, но есть в currentCourses
        courseToSpecialtyTable.deleteWhere{
            (courseToSpecialtyTable.specialtyId eq id) and (courseToSpecialtyTable.courseId inList coursesToDeleteId)
        }

        // если появился новый курс, мы его добавим
        courseToSpecialtyTable.batchInsert(coursesToAdd){course->
            this[courseToSpecialtyTable.specialtyId] = id
            this[courseToSpecialtyTable.courseId] = course.courseId
            this[courseToSpecialtyTable.term] = course.term
        }
    }

}
