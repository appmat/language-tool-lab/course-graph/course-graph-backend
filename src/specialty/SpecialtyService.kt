package ru.appmat.specialty

import org.jetbrains.exposed.sql.Slf4jSqlDebugLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction

class SpecialtyService(private val specialtyDAO: SpecialtyDAO) {
    fun create(createCommand: CreateSpecialtyCommand) = transaction {
        val specialtyId = specialtyDAO.persist(SpecialtyInfo(-1, createCommand.name, createCommand.facId))
        specialtyDAO.load(specialtyId)
    }

    fun list() = transaction {
        specialtyDAO.list()
    }

    fun load(id: Long) = transaction {
        specialtyDAO.load(id)
    }

    fun delete(id: Long) = transaction {
        specialtyDAO.delete(id)
    }

    fun updateInfo(specialty: SpecialtyInfo) = transaction {
        specialtyDAO.updateInfo(specialty)
    }

    // связи
    fun loadByCourse(courseId: Long) = transaction {
        specialtyDAO.loadByCourse(courseId)
    }

    fun updateCourses(id: Long, courses: Array<SpecialtyCourse>) = transaction {
        addLogger(Slf4jSqlDebugLogger)
        specialtyDAO.updateCourses(id, courses)
    }

    fun loadByFaculty(facId:Long) = transaction {
        specialtyDAO.loadByFaculty(facId)
    }

}