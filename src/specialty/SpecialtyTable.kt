package ru.appmat.specialty

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.Column

object SpecialtyTable : LongIdTable("specialty") {
    val name: Column<String> = varchar("name", 150)
    val facId: Column<Long> = long("fac_id")
}