package ru.appmat.specialty

data class SpecialtyInfo(val id: Long, val name: String, val facId: Long)
data class SpecialtyCourse(val courseId: Long, val term: Long)

data class Specialty(val specialty: SpecialtyInfo, val specialtyCourses: Set<SpecialtyCourse>)

data class CreateSpecialtyCommand(val name: String, val facId: Long)