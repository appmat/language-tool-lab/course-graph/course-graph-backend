package ru.appmat.specialty

import org.jetbrains.exposed.sql.Table

object CourseToSpecialtyTable : Table("specialty_to_course") {
    val courseId = long("course_id")
    val specialtyId = reference("specialty_id", SpecialtyTable)
    val term= long("term")
}