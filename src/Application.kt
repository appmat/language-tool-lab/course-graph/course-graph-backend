package ru.appmat

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.metrics.micrometer.*
import io.ktor.response.*
import io.ktor.routing.*
import io.micrometer.core.instrument.binder.jvm.*
import io.micrometer.core.instrument.binder.logging.LogbackMetrics
import io.micrometer.core.instrument.binder.system.ProcessorMetrics
import io.micrometer.core.instrument.binder.system.UptimeMetrics
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import io.sentry.Sentry
import ktor_health_check.Health
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.postgresql.PGProperty

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(Locations) {
    }
    install(ContentNegotiation) {
        gson {}
    }
    install(CallLogging)
    install(CORS) {
        allowNonSimpleContentTypes = true

        method(HttpMethod.Options)
        method(HttpMethod.Delete)
        method(HttpMethod.Put)

        host("localhost:3000")
        //anyHost()
        //host("www.test-cors.org", listOf("https"))
    }

    val hikariDataSource = HikariDataSource(
        HikariConfig().apply {
            val hikariAppConfig = environment.config.config("ktor.hikari")
            maximumPoolSize = hikariAppConfig.property("maximum-pool-size").getString().toInt()
            addDataSourceProperty(PGProperty.SSL_MODE.getName(), hikariAppConfig.property("ssl-mode").getString())

            when (val gitlabDatabaseUrl = hikariAppConfig.propertyOrNull("gitlab-database-url")) {
                null -> {
                    username = hikariAppConfig.property("user").getString()
                    password = hikariAppConfig.property("password").getString()
                    jdbcUrl = hikariAppConfig.property("jdbc-url").getString()
                }
                else -> {
                    // see https://docs.gitlab.com/ee/topics/autodevops/customize.html#using-external-postgresql-database-providers
                    val gitlabDatabaseUrlRegex = Regex("^postgres://(?<username>[^:]*):(?<password>[^@]*)@(?<address>.*)$")
                    val matchResult = requireNotNull(gitlabDatabaseUrlRegex.matchEntire(gitlabDatabaseUrl.getString())) {
                        """Database url should have format "postgres://user:password@host:port/database""""
                    }
                    val (user, pass, address) = matchResult.destructured
                    username = user
                    password = pass
                    jdbcUrl = "jdbc:postgresql://${address}"
                }

            }
        }
    )

    install(Health) {
        readyCheck("database", hikariDataSource::isRunning)
    }

    Sentry.init {
        it.isEnableExternalConfiguration = true
    }

    // Metrics
    val appMicrometerRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

    install(MicrometerMetrics) {
        registry = appMicrometerRegistry
        meterBinders = listOf(
            JvmHeapPressureMetrics(),
            JvmThreadMetrics(),
            JvmMemoryMetrics(),
            JvmInfoMetrics(),
            JvmGcMetrics(),
            ProcessorMetrics(),
            LogbackMetrics(),
            UptimeMetrics()
        )
    }

    routing {
        get("/metrics") {
            call.respond(appMicrometerRegistry.scrape())
        }
    }


    Flyway.configure().dataSource(hikariDataSource).load().migrate()
    Database.connect(hikariDataSource)
}


