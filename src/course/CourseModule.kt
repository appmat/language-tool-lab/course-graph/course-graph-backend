package ru.appmat.course

import io.ktor.application.*
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.Routing
import io.ktor.routing.routing
import org.jetbrains.exposed.sql.transactions.transaction
import ru.appmat.API

@Suppress("unused") // Referenced in application.conf
fun Application.courseModule() {
    val courseService = CourseService(CourseDAO(CourseTable, SpecialtyToCourseTable))
    routing {
        course(courseService)
    }
}

internal fun Routing.course(courseService: CourseService) {
    get<API.Courses> {
        val courseList = courseService.list()
        call.respond(courseList)
    }
    post<API.Courses> {
        val createCourseCommand = call.receive<CreateCourseCommand>()
        val createdCourse = courseService.create(createCourseCommand)
        call.respond(Created, createdCourse)
    }
    get<API.Courses.Course> {
        val course = courseService.load(it.id)
        call.respond(course)
    }
    put<API.Courses.Course> {
        val course = call.receive<Course>()
        courseService.update(course)
        call.respond(OK)
    }
    delete<API.Courses.Course> {
        courseService.delete(it.id)
        call.respond(OK)
    }

    // работа со связями
    get<API.Specialties.Specialty.Courses> {
        val courses = courseService.loadBySpecialty(it.parent.id)
        call.respond(courses)
    }


}
