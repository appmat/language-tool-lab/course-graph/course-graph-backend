package ru.appmat.course

data class Course(val id:Long,val name: String)

data class CreateCourseCommand(val name: String)