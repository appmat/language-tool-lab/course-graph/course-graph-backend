package ru.appmat.course

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder

class CourseDAO(
        private val courseTable: CourseTable,
        private val specialtyToCourseTable: SpecialtyToCourseTable
) {
    private fun ResultRow.toCourse() = Course(
            id = this[CourseTable.id].value,
            name = this[CourseTable.name]
    )

    private fun UpdateBuilder<*>.fromCourse(course: Course) {
        this[CourseTable.name] = course.name
    }

    fun load(id: Long) = courseTable.select {
        CourseTable.id eq id
    }.single().toCourse()

    fun list() = courseTable.selectAll().mapNotNull {
        it.toCourse()
    }

    fun persist(course: Course) = courseTable.insertAndGetId {
        it[name] = course.name
    }.value

    fun update(course: Course) = courseTable.update({
        CourseTable.id eq course.id
    }) {
        it.fromCourse(course)
    }

    fun delete(id: Long) = courseTable.deleteWhere {
        CourseTable.id eq id
    }

    // работа со связями
    fun loadBySpecialty(specialtyId: Long) = (specialtyToCourseTable innerJoin courseTable)
        .slice(courseTable.columns)
        .select {
            SpecialtyToCourseTable.specialtyId eq specialtyId
        }.mapNotNull {
            it.toCourse()
        }
}