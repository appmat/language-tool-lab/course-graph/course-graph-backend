package ru.appmat.course

import org.jetbrains.exposed.sql.transactions.transaction

class CourseService(private val courseDAO: CourseDAO) {
    fun create(createCommand: CreateCourseCommand) = transaction {
        val courseId = courseDAO.persist(Course(-1, createCommand.name))
        courseDAO.load(courseId)
    }
    fun list()= transaction {
        courseDAO.list()
    }
    fun load(id: Long) = transaction {
        courseDAO.load(id)
    }
    fun delete(id: Long) = transaction {
        courseDAO.delete(id)
    }

    fun update(course: Course) = transaction {
        courseDAO.update(course)
    }

    //связи
    fun loadBySpecialty(specialtyId: Long) = transaction {
        courseDAO.loadBySpecialty(specialtyId)
    }

}