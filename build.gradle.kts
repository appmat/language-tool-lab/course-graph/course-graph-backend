import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project

val exposed_version: String by project
val flyway_version: String by project
val postgresql_version: String by project
val hikari_version: String by project
val prometheus_version:String by project
val sentry_version: String by project

plugins {
    application
    kotlin("jvm") version "1.5.20"
}

group = "ru.appmat"
version = "0.0.1"

application {
    mainClassName = "io.ktor.server.netty.EngineMain"
}

repositories {
    mavenLocal()
    jcenter()
    maven { url = uri("https://kotlin.bintray.com/ktor") }
    maven(url = "https://jitpack.io")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("io.ktor:ktor-gson:$ktor_version")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")

    implementation("org.flywaydb:flyway-core:$flyway_version") //
    implementation("org.jetbrains.exposed:exposed-core:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-dao:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposed_version")

    implementation("org.postgresql:postgresql:$postgresql_version") //
    implementation("com.zaxxer:HikariCP:$hikari_version") //

    // Health check
    implementation("com.github.zensum:ktor-health-check:011a5a8")

    // Sentry monitoring
    implementation("io.sentry:sentry-logback:$sentry_version")

    // Metrics
    implementation("io.ktor:ktor-metrics-micrometer:$ktor_version")
    implementation("io.micrometer:micrometer-registry-prometheus:$prometheus_version")

}

kotlin.sourceSets["main"].kotlin.srcDirs("src")
kotlin.sourceSets["test"].kotlin.srcDirs("test")

sourceSets["main"].resources.srcDirs("resources")
sourceSets["test"].resources.srcDirs("testresources")

/**
 * stage task is executed in buildpack jvm pipeline
 * @see <a href="https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku">https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku</a>
 */
tasks.register("stage") {
    dependsOn("installDist")
    /**
     * generates the Procfile that specifies a command to run the application
     */
    doLast {
        val relativeInstallationPath = tasks.installDist.get().destinationDir.relativeTo(project.projectDir)
        File("Procfile").writeText(
            """
                web: ./$relativeInstallationPath/bin/${project.name}
            """.trimIndent()
        )
    }
}

tasks.withType<KotlinCompile>().all {
    kotlinOptions.freeCompilerArgs += "-Xopt-in=io.ktor.util.KtorExperimentalAPI"
    kotlinOptions.freeCompilerArgs += "-Xopt-in=io.ktor.locations.KtorExperimentalLocationsAPI"
}

